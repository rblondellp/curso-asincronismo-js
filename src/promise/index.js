const promise = new Promise((resolve, reject) => {
    resolve("OK");
});

const cows = 15;

const countCows = new Promise((resolve, reject) => {
    if (cows > 10) {
        resolve("OK");
    } else {
        reject("reject");
    }
});

countCows
    .then(result => console.log(result))
    .catch(error => console.error(error))
    .finally(() => console.log("Finally"));
