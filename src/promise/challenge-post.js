import fetch from 'node-fetch';
const API = 'https://api.escuelajs.co/api/v1';

function postData(urlApi, data) {
    const response = fetch(urlApi, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });

    return response;
}

const datos = {
    title: 'prueba',
    price: 999,
    description: 'Description',
    categoryId: 1,
    images: ['https://placeimg.com/640/480/any'],
};

postData(`${API}/products`, datos)
    .then(response => response.json)
    .then(datos => console.log(datos))
    .catch(error => console.log(error));
